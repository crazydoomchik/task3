﻿using System;
using System.IO;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            string path;
            bool cycle = true;

            while (cycle)
            {
                SearchMaxSum searchMaxSum = new SearchMaxSum();

                if (args.Length != 0)
                {
                    path = args[0];
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Enter path to file");
                    path = Console.ReadLine();
                }

                if (!File.Exists(path))
                {
                    Console.WriteLine($"Error: Enter correct path!");
                    continue;
                }

                searchMaxSum.ParseFile(path);
                

                foreach (var item in searchMaxSum.IncorrectLineRepository)
                {
                    Console.WriteLine($"Error: Line number: {item.Key}; line: {item.Value}");
                }

                try 
                { 
                    var lineMaxSum = searchMaxSum.GetLineAndMaxSum();
                    Console.WriteLine($"Line number: {lineMaxSum.LineNumber} Max Sum: {lineMaxSum.MaxSum}");
                }
                catch(InvalidOperationException exe)
                {
                    Console.WriteLine(exe.Message);
                }


            }
        }
    }
}
