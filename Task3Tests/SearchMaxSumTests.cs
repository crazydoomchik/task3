﻿
using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

namespace Task3.Tests
{

    public class SearchMaxSumTests
    {

        [Test()]
        [TestCase("NumberList.txt", 806.065)]
        [TestCase("TestList.txt", 2000)]
        [TestCase("FileDoesNotExist.txt", 100)]
        [TestCase("NumberList2.txt", 1)]
        public void LineWhithMaxSumTest(string path, double expectedSum)
        {

                //Act
            SearchMaxSum search = new SearchMaxSum();

            try
            {
                search.ParseFile(path);

                var result = search.GetLineAndMaxSum().MaxSum;

                //Assert
                Assert.AreEqual(expectedSum, result);
            }
            catch (FileNotFoundException exe)
            {
                StringAssert.Contains(exe.Message, "Unable to find the specified file.");
            }
            catch (InvalidOperationException exe)
            {
                StringAssert.Contains(exe.Message, "File does not have correct line.");
            }

        }

        [Test()]
        [TestCase("34.3,43,342.4,234,1,67,45.3", true)]
        [TestCase("334.3,43,342.4,234,1,6d7,4f5.3", false)]
        [TestCase("34.3,43,342.4,234,1,617,45.3", true)]
        [TestCase("34.3,43,342.4,24,1,67,45.3", true)]
        [TestCase("a", false)]
        public void ParseLineTest(string line, bool expected)
        {

            SearchMaxSum parse = new SearchMaxSum();
            bool inputLine = parse.ParseLineAndGetSum(line, out double sum);

            //Assert
            Assert.AreEqual(expected, inputLine);

        }

        [Test()]
        [TestCaseSource(nameof(MySourceMethod))]
        public void ParseFileTest(string path, Dictionary<int, string> expected)
        {

            //Act
            SearchMaxSum searchMaxSum = new SearchMaxSum();
            try
            {
                searchMaxSum.ParseFile(path);
            }
            catch (FileNotFoundException exe)
            {
                StringAssert.Contains(exe.Message, "Unable to find the specified file.");
                return;
            }

            if (searchMaxSum.IncorrectLineRepository.Count == 0)
            {
                Assert.Fail("Incorrect repository is empty");
                return;
            }

            CollectionAssert.AreEqual(expected, searchMaxSum.IncorrectLineRepository);
        }

        static IEnumerable<object[]> MySourceMethod()
        {
            Dictionary<int, string> expected = new Dictionary<int, string>
            {
                {2, "34.3,43,342.4,234,1,6d7,4f5.3" },
                {7, "a34.3,43,342.4,234,1,67,45.3" },
                {8, "a"},
            };
            
            return new[] { new object[] { "NumberList.txt",  expected }, };
        }

    }
}