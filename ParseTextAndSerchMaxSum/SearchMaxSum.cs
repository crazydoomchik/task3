﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;


[assembly: InternalsVisibleTo("Task3Tests")]
namespace Task3
{
    public class SearchMaxSum
    {

        private  Dictionary<int, double> sumRepository = new Dictionary<int, double>();

        private Dictionary<int, string> incorrectLineRepository = new Dictionary<int, string>();
        public Dictionary<int, string> IncorrectLineRepository
        {
            get
            {
                return incorrectLineRepository;
            }
        }

        public void ParseFile(string path)
        {
            
            if (!File.Exists(path))
            {
                throw new FileNotFoundException();
            }

            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                
                string line;
                int lineNumber = 0;
                
                while ((line = sr.ReadLine()) != null)
                {
                    double sum = 0;
                    lineNumber++;

                    if(ParseLineAndGetSum(line, out sum))
                    {
                        sumRepository.Add(lineNumber, sum);
                    }
                    else
                    {
                        incorrectLineRepository.Add(lineNumber, line);
                    }

                }
            }
            
        }

        internal bool ParseLineAndGetSum(string line, out double sum)
        {
            sum = 0;

            try
            {
                string[] currentLine = line.Split(",", StringSplitOptions.RemoveEmptyEntries);
                if (currentLine.Length == 0)
                {
                    return false;
                }
                foreach (var item in currentLine)
                {
                    sum += Double.Parse(item, new NumberFormatInfo());                   
                }
                return true;
            }
            catch(Exception exe)
            {
                Log.Write(exe);
            }
            return false;
        }

        public LineWithMaxSum GetLineAndMaxSum()
        {
            int lineNumber = 0;
            double maxSum = 0;

            if (sumRepository.Count != 0)
            {
                maxSum = sumRepository.Values.Max();
                lineNumber = sumRepository.FirstOrDefault(x => x.Value == maxSum).Key;
            }
            else
            {
                throw new InvalidOperationException("File does not have correct line.");
            }

            return new LineWithMaxSum { LineNumber = lineNumber, MaxSum = maxSum };
            
        }

    }
}
